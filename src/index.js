import reactDOM from 'react-dom/client'
import App from './components/app'
import 'react-toastify/dist/ReactToastify.css';


import store from './redux/store'
import { Provider } from 'react-redux'

const ele = document.querySelector('#root')
const root = reactDOM.createRoot(ele)

root.render(

   <Provider store={ store }>
     <App />
   </Provider>

)