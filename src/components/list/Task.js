
export default function Task(props) {

  const taskActivated = (e) => {
    props.onTaskRemove(e)
  }
  


  return (
    <div>
  {props.list.map((e, i) => {
    return (
      e.completed === false && (
        <div className="TaskFather">
          <div key={e._id} className="Task" onClick={() => props.slideOpener(e)}>
            <span>{e.taskValue}</span>
          </div>
          <div>
            <input type="checkbox" className="inputCheck" />
            <span onClick={() => taskActivated(e)} className="check" ></span>
          </div>
        </div>
      )
    );
  })}
</div>

  )
}
